/**
=========================================================
* Material Dashboard 2  React - v2.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

import { useMemo } from "react";

// porp-types is a library for typechecking of props
import PropTypes from "prop-types";

// react-chartjs-2 components
import { Bar } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";

// @mui material components
import Card from "@mui/material/Card";
import Divider from "@mui/material/Divider";
import { BsDot } from "react-icons/bs";
// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

// ReportsBarChart configurations
import configs from "examples/Charts/BarCharts/ReportsBarChart/configs";

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

function ReportsBarChart({ color, title, atmosphericData, date, chart }) {
  const { data, options } = configs(chart.labels || [], chart.datasets || {});
  const { humidity, wind_gust } = atmosphericData;
  console.log("humidity", humidity);
  return (
    <Card sx={{ height: "100%", borderRadius: "0" }}>
      <MDBox padding="1rem">
        {useMemo(
          () => (
            <MDBox
              variant="gradient"
              bgColor={color}
              borderRadius="xs"
              coloredShadow={color}
              py={2}
              pr={0.5}
              mt={0}
              height="12.5rem"
            >
              <Bar data={data} options={options} redraw />
            </MDBox>
          ),
          [color, chart]
        )}
        <MDBox pt={3} pb={1} px={1}>
          <MDTypography variant="h6" fontWeight="regular" textTransform="capitalize">
            {title}
          </MDTypography>

          <MDBox display="flex" justifyContent="space-between">
            <MDTypography component="div" variant="button" color="text">
              Humidity
            </MDTypography>
            <MDTypography variant="button" fontWeight="bold" color="text">
              0%
            </MDTypography>
          </MDBox>
          <MDBox display="flex" justifyContent="space-between">
            <MDTypography component="div" variant="button" color="text">
              Wind gust
            </MDTypography>
            <MDTypography variant="button" fontWeight="bold" color="text">
              13.7 km/h at 1 pm
            </MDTypography>
          </MDBox>

          {/* <MDBox lineHeight={0}>
            <MDTypography component="div" variant="button" color="text">
              Wind gusts &nbsp;&nbsp;&nbsp;
              <MDTypography variant="button" fontWeight="bold" textTransform="capitalize">
                {"company"}
              </MDTypography>
            </MDTypography>
          </MDBox>
          <MDTypography component="div" variant="button" color="text" fontWeight="light">
            {"Humidity"} {humidity}
          </MDTypography>
          <MDTypography component="div" variant="button" color="text" fontWeight="light">
            {wind_gust}
          </MDTypography> */}
          <Divider sx={{ background: "black" }} />
          <MDBox display="flex" alignItems="center">
            <MDTypography variant="button" color="text" lineHeight={1} sx={{ mt: 0.15, mr: 0.5 }}>
              66.7 ac
            </MDTypography>
            <MDTypography variant="button" color="text" lineHeight={1} sx={{ mt: 0.15, mr: 0.5 }}>
              <BsDot />
            </MDTypography>
            <MDTypography variant="button" color="text" fontWeight="light">
              {date}
            </MDTypography>
          </MDBox>
        </MDBox>
      </MDBox>
    </Card>
  );
}

// Setting default values for the props of ReportsBarChart
ReportsBarChart.defaultProps = {
  color: "info",
  description: "",
};

// Typechecking props for the ReportsBarChart
ReportsBarChart.propTypes = {
  color: PropTypes.oneOf(["primary", "secondary", "info", "success", "warning", "error", "dark"]),
  title: PropTypes.string.isRequired,
  atmosphericData: PropTypes.arrayOf(PropTypes.string),
  date: PropTypes.string.isRequired,
  chart: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.array, PropTypes.object])).isRequired,
};

export default ReportsBarChart;
